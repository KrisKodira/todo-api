package todorestapi.exception;

public class ToDoNotFoundException extends Exception {
    private String todo_id;
    public ToDoNotFoundException(String todo_id) {
        super(String.format("ToDo is not found with id : '%s'", todo_id));
    }
}
