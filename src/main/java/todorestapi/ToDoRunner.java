package todorestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToDoRunner {
    public static void main(String[] args) {
        SpringApplication.run(ToDoRunner.class, args);
    }
}
