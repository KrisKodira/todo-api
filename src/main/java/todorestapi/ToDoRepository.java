package todorestapi;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ToDoRepository extends MongoRepository<ToDo, String> {

    public ToDo findByTitle(String title);
    public ToDo findByIsDone(Boolean isDone);

}
