package todorestapi;

import todorestapi.exception.ToDoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;

import java.util.List;


@RestController
public class ToDoController {

    @Autowired
    public ToDoRepository repository;

    @CrossOrigin(origins = "*")
    @GetMapping("/todos")
    public List<ToDo> getAllToDos() {
        return repository.findAll();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/todos")
    public ToDo createToDo(@Valid @RequestBody ToDo todo) {
        return repository.save(todo);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/todos/{id}")
    public ToDo getToDoById(@PathVariable(value = "id") String todoId) throws ToDoNotFoundException {
        return repository.findById(todoId)
                .orElseThrow(() -> new ToDoNotFoundException(todoId));
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/todos/{id}")
    public ToDo updateToDo(@PathVariable(value = "id") String todoId,
                           @Valid @RequestBody ToDo todoDetails) throws ToDoNotFoundException {

        ToDo todo = repository.findById(todoId)
                .orElseThrow(() -> new ToDoNotFoundException(todoId));

        todo.setTitle(todoDetails.getTitle());
        todo.setIsDone(todoDetails.getIsDone());

        ToDo updatedToDo = repository.save(todo);

        return updatedToDo;
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/todos/{id}")
    public ResponseEntity<?> deleteToDo(@PathVariable(value = "id") String todoId) throws ToDoNotFoundException {
        ToDo todo = repository.findById(todoId)
                .orElseThrow(() -> new ToDoNotFoundException(todoId));

        repository.delete(todo);

        return ResponseEntity.ok().build();
    }

}
