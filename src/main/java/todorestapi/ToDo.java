package todorestapi;

import org.springframework.data.annotation.Id;

public class ToDo {

    @Id
    public String id;

    public String title;
    public Boolean isDone;

    public void ToDo() {}

    public ToDo(String title, Boolean isDone) {
        this.title = title;
        this.isDone = isDone;
    }

    @Override
    public String toString() {
        return String.format(
                "ToDo[id=%s, title='%s', isDone='%s']",
                id, title, isDone);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }
}
